<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,10) as $i){
            DB::table('mahasiswa')->insert([
                'nim'       => $faker->ean8,
                'nama'      => $faker->name,
                'jurusan'   => $faker->company,
                'email'     => $faker->email,
            ]);
        }
    }
}
