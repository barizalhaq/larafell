<?php
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'WelcomeController@index');
Route::get('/download', 'WelcomeController@download')->name('download');

Auth::routes();

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/edit-password', 'ProfileController@showEditPass')->name('edit_password');
Route::post('/edit-password', 'ProfileController@editPass')->name('edit_password');

Route::prefix('mahasiswa')->group(function() {
    Route::get('input-data', 'MahasiswaController@show')->name('input');
    Route::post('input-data', 'MahasiswaController@store')->name('input');
    Route::get('{id}', 'MahasiswaController@showEdit')->name('edit');
    Route::put('{id}', 'MahasiswaController@update')->name('edit');
    Route::delete('delete/{id}', 'MahasiswaController@destroy')->name('delete');
});