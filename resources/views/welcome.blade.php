@extends('layouts.larafell')


@section('meta')
    <link rel="stylesheet" href="/css/welcome_style.css">
@endsection


@section('content')
    <section>
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1 class="display-4">Larafell</h1>
                        <p class="lead">Sistem auth, dan CRUD</p>
                        <hr>
                        @if(Route::has('login'))
                            @auth
                                <a class="btn btn-light btn-lg" href="{{ url('/profile') }}" role="button">Profile</a>
                        @else
                            <a class="btn btn-light btn-lg" href="{{ route('login') }}" role="button">Login</a>
                            @if(Route::has('register'))
                                <a class="btn btn-light btn-lg" href="{{ route('register') }}" role="button">Register</a>
                            @endif
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-auto">
                    <button class="btn btn-lg btn-primary show">Tampilkan Data</button>
                </div>
                <div class="col-lg-auto">
                    <form action="{{ route('input') }}" method="get">
                        <button type="submit" class="btn btn-lg btn-primary">Tambah Data</button>
                    </form>
                </div>
                <div class="col-lg-auto">
                    <form action="{{ route('download') }}" method="get">
                        <button type="submit" class="btn btn-lg btn-primary">Download Data</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="container data-mhs">
        <div class="table-responsive">
            <table class="table table-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Jurusan</th>
                    <th scope="col">Action</th>
                </tr>
                    @foreach($mahasiswa as $mhs)
                    <tr>
                        <th scope="row">{!! $index++ !!}</th>
                        <td>{{ $mhs->nim }}</td>
                        <td>{{ $mhs->nama }}</td>
                        <td>{{ $mhs->email }}</td>
                        <td>{{ $mhs->jurusan }}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                <form action="{{ route('edit', [$mhs->id]) }}" method="get">
                                    <button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#editModal">Edit</button>
                                </form>
                                    
                                <form action="{{ route('edit', [$mhs->id]) }}" method="post">
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus?')">Delete</button>
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>

    {{-- <section class="modal-box-edit"> --}}
        <!-- <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Mahasiswa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                    <form action="/" method="post">
                        <div class="form-group">
                            <label>NIM</label>
                            <input type="text" class="form-control" name="nim">
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label>Jurusan</label>
                            <input type="text" class="form-control" name="jurusan">
                        </div>
                        {{-- {!! csrf_field() !!} --}}
                        <input type="hidden" name="_method" value="put"> 
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </form>
                    </div>
                </div>
            </div>
        </div> -->
    {{-- </section> --}}
@endsection


@section('script')
    <script src="/js/welcome_js.js"></script>
@endsection