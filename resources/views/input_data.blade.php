@extends('layouts.larafell')


@section('meta')
    <link rel="stylesheet" href="/css/input_style.css">
@endsection


@section('content')
    <section>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Larafell</h1>
                <p class="lead">Sistem auth, dan CRUD</p>
                <hr>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                        <h5 class="card-title">Input Data Mahasiswa</h5>
                        @if(count($errors) > 0)
                                @foreach($errors->all() as $err)
                                    <span class="badge badge-danger d-block text-left">{{ $err }}</span>
                                @endforeach
                        @endif
                            <form action="{{ route('input') }}" method="post">
                                <div class="form-group">
                                    <label>NIM</label>
                                    <input type="text" class="form-control" name="nim">
                                </div>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Jurusan</label>
                                    <input type="text" class="form-control" name="jurusan">
                                </div>
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="post"> 
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script src="/js/input_js.js"></script>
@endsection