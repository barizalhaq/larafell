<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;

class MahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('input_data');
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'nim'       => 'required|digits:8',
            'nama'      => 'required|min:3|string|regex:/^[\pL\s\-]+$/u',
            'jurusan'   => 'required|string|regex:/^[\pL\s\-]+$/u',
            'email'     => 'required|email|unique:mahasiswa'
        ]);

        $mhs            = new Mahasiswa;
        $mhs->nim       = $req->nim;
        $mhs->nama      = $req->nama;
        $mhs->jurusan   = $req->jurusan;
        $mhs->email     = $req->email;
        $mhs->save();
        
        return redirect('/');
    }

    public function showEdit($id)
    {
        $mhs  = Mahasiswa::findOrFail($id);
        return view('edit_data', compact('mhs'));
    }

    public function update($id, Request $req)
    {
        $mhs  = Mahasiswa::findOrFail($id);

        $this->validate($req, [
            'nim'       => 'required|digits:8',
            'nama'      => 'required|min:3|string|regex:/^[\pL\s\-]+$/u',
            'jurusan'   => 'required|string|regex:/^[\pL\s\-]+$/u',
            'email'     => 'required|string|email|max:255|unique:mahasiswa,email,'.$mhs->id,
        ]);

        $mhs->nim       = $req->nim;
        $mhs->nama      = $req->nama;
        $mhs->jurusan   = $req->jurusan;
        $mhs->email     = $req->email;
        $mhs->save();
        
        return redirect('/');
    }

    public function destroy($id)
    {
        $mhs    = Mahasiswa::findOrFail($id);
        $mhs->delete();

        return redirect('/');
    }
}
