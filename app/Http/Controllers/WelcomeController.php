<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Mahasiswa;

class WelcomeController extends Controller
{
    public function index()
    {
        $mahasiswa  = Mahasiswa::all()->sortBy('id');
        $index      = 1;   
        return view('welcome', compact('mahasiswa', 'index'));
    }

    public function download()
    {
        $file       = public_path(). "/download/Designing a Business Process Architecture 2.pdf";
        $filename   = "Designing a Business Process Architecture 2.pdf";
        $headers = [
            'Content-Type' => 'application/pdf',
         ];

         return response()->download(storage_path("app/public/download/{$filename}"));
    }
}
