<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile');
    }

    public function showEditPass()
    {
        return view('auth.passwords.edit_password');
    }

    public function editPass(Request $req)
    {
        if(!(Hash::check($req->get('current-password'), Auth::user()->password)))
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");

        if(strcmp($req->get('current-password'), $req->get('new-password')) == 0)
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");

        $validatedData = $req->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        $user   = Auth::user();
        $user->password = bcrypt($req->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");
    
    }
}
